struct dateRec
{
	int day;
	int month;
	int year;
};

struct itemBD
{
	char name[255];
	char address[255];
	dateRec openingDate;
	int countBed;
};

int cinMenu();
void menuItemSelectable(int position);
int cinInteger();
void blockWork();
void coutItemBD(itemBD temp,int numbItem);
void showItem(bool block);
void addItem();
void rmItem();
void changeItem();
itemBD cinInfo(bool skip);
dateRec cinDate();

bool writeNewItem(itemBD newItem,const char *FName);
bool delItem(int numbDel,const char *FName);
bool writeChangeItem(itemBD newItem,int numbChange,const char *FName);