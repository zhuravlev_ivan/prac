#include <iostream>
#include <cstdlib>
#include "functions.h"

using namespace std;

int cinMenu() {
		system("clear");
		cout << "\t\tВы запустили программу PR2_BD" << endl;
		cout << endl << "\tВыберите пункт меню:" << endl;
		cout << "\t1 - Просматреть БД" << endl;
		cout << "\t2 - Добавить запись" << endl;
		cout << "\t3 - Удалить запись" << endl;
		cout << "\t4 - Изменить запись" << endl;
		cout << "\t5 - Выйти" << endl;
	return cinInteger();
}

void menuItemSelectable(int position) {
	switch (position) {
		case 1:
			showItem(true);
		break;
		case 2:
			addItem();
		break;
		case 3:
			rmItem();
		break;
		case 4:
			changeItem();
		break;
		case 5:		
		break;
		default:
			cout << "Выберите существующий пункт меню!" << endl;
			blockWork();
	}
} 